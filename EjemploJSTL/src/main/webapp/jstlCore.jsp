<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL Core</title>
    </head>
    <body>
        <h1>JSTL Core</h1>
        <!-- Manipulaci�n de Variables -->
        <c:set var="nombre" value="Ernesto" />
        <!-- Se define la Variable -->
        Variable nombre: <c:out value="${nombre}" />
        <br>
        <br>
        Variable con c�digo HTML
        <c:out value="<h4>Hola</h4>" escapeXml="false"/>
        <br>
        <br>
        <!-- C�digo condicionado, uso de if -->
        <c:set var="bandera" value="true" />

        <c:if test="${bandera}" >
            La bandera es verdadera
        </c:if>
        <br>
        <c:if test="${param.opcion != null}">
            <c:choose>
                <c:when test="${param.opcion == 1}">
                    <br>
                    Opci�n 1 seleccionada
                </c:when>
                <c:when test="${param.opcion == 2}">
                    <br>
                    Opci�n 2 seleccionada
                </c:when>
                <c:otherwise>
                    <br>
                    Opci�n proporcionada desconocida: ${param.opcion}
                </c:otherwise>
            </c:choose>
        </c:if>
        <%
            String nombres[] = {"Claudia", "Pedro", "Juan"};
            request.setAttribute("nombres", nombres);
        %>

        <br>
        <br>
        Lista de Nombres:
        <br>
        <ul>
            <c:forEach var="persona" items="${nombres}">
                <li>${persona}</li>
            </c:forEach>
        </ul>
        <br>
        <a href='index.jsp'>Regreso al Inicio</a>
    </body>
</html>
