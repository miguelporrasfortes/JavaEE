package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/CarritoServlet")
public class CarritoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        //Creamos o recuperamos HttpSession
        HttpSession sesion = request.getSession();

        //Recuperar la lista de artículos previos
        List<String> articulos = (List<String>) sesion.getAttribute("articulos");

        //Verificamos si la lista de artículos existe
        if (articulos == null) {
            articulos = new ArrayList<>();
            sesion.setAttribute("articulos", articulos);
        }

        //Procesamos el nuevo artículo
        String articuloNuevo = request.getParameter("articulo");

        //Revisamos y agregamos el artículo nuevo
        if (articuloNuevo != null && !articuloNuevo.trim().equals("")) {
            articulos.add(articuloNuevo);
        }

        try ( //Imprimimos la lista de artículos
                PrintWriter out = response.getWriter()) {
            out.print("<h1>Lista de artículos </h1>");
            out.print("<br>");
            //Iteramos todos los artículos
            for (String articulo : articulos) {
                out.print("<li>" + articulo + "</li>");
            }

            //agregamos link de retorno a Inicio
            out.print("<br>");
            out.print("<a href='/EjemploCarritoCompras'> Regresar al Inicio </a>");
        }
    }
}
