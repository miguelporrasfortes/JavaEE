package controlador;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import modelo.Rectangulo;

@WebServlet("/ServletControlador")
public class ServletControlador extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1. Procesamos parçametros
        String accion = request.getParameter("accion");

        //2. Creamos los JavaBeans
        Rectangulo recRequest = new Rectangulo(1, 2);
        Rectangulo recSesion = new Rectangulo(3, 4);
        Rectangulo recAplicacion = new Rectangulo(5, 6);

        //3. Agregamos al javabean un alcance
        //Revisamos la acción proporcionada
        if ("agregarVariables".equals(accion)) {
            //Alcance request
            request.setAttribute("rectanguloRequest", recRequest);
            //Alcance session
            HttpSession sesion = request.getSession();
            sesion.setAttribute("rectanguloSesion", recSesion);

            //Alcance application
            ServletContext application = this.getServletContext();
            application.setAttribute("rectanguloApplication", recAplicacion);

            //Agregamos un mensaje
            request.setAttribute("mensaje", "Las variables fueron agregadas");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else if ("listarVariables".equals(accion)) {
            //4.Redireccionamos al JSP que despliega las Variables
            request.getRequestDispatcher("WEB-INF/alcanceVariables.jsp").forward(request, response);
        } else {
            //4.Redireccionamos a la página de Inicio
            request.setAttribute("mensaje", "Acción no proporcionada");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            //Esta línea no agregaría información al JSP
            //response.sendRedirect("index.jsp");
        }
    }
}
