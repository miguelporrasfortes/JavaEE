<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Scriptlets JSP</title>
    </head>
    <body>
        <h1>JSP con Scriptlets</h1>
        <br>
        <%-- Scriptlet para enviar informaci�n al navegador --%>
        <% 
            out.print("Saludos desde un Scriptlet");
        %>
        <%-- Scriptlet para controlar los objetos impl�citos --%>
        <%
           String nombreAplicacion = request.getContextPath();
           out.print("nombre de la aplicaci�n:" + nombreAplicacion);
        %>
        <br
        <%-- Scriptlet con c�digo condicional --%>
        <% 
            if(session != null && session.isNew()) {
        %>
        La sesi�n S� es nueva
        <%
            }else if(session != null) {
        %>
        La sesi�n NO es nueva
        
        <% } %>
        <br>
        <a href="index.html">Regresar a inicio</a>
    </body>
</html>
